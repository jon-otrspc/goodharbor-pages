let width, height, cx, cy;
let svg = document.getElementById('svg');

let conf = {
  resY: 10,
  resX: 10,
  noiseCoef: 0.004,
  mouseCoef: 0.0,
  timeCoef: .00003,
  noise4: .0015,
  heightCoef: 50 };

let paths = [];
const simplex = new SimplexNoise();
let colors = ['#03345E'];
let cscale = chroma.scale(colors);
let mouseX = 0,mouseY = 0;

function init() {
  $("#svg").html("");
  paths = [];
  const r = svg.getBoundingClientRect();
  width = $(window).width();

  height = r.height;
  height = $(window).height();
  cx = width / 2;
  cy = height / 2;
 // onResize();
  // window.addEventListener('resize', onResize, false);

  const nx = Math.round(width / conf.resX);
  const ny = Math.round((height - conf.heightCoef * 2) / conf.resY);
  let points, path;
  for (let j = 0; j < ny; j++) {
    points = [];
    for (let i = 0; i < nx; i++) {
      points.push([i * conf.resX, conf.heightCoef + j * conf.resY]);
    }
    points.push([width, conf.heightCoef + j * conf.resY]);
    path = new Path(svg, points, '#03345E');
    paths.push(path);
  }

  animate();

/*  
  svg.addEventListener('mouseup', e => {
    updateStrokes();animateNoise();});
  document.body.addEventListener('mousemove', e => {
    const r = e.target.getBoundingClientRect();
    mouseX = conf.mouseCoef * (e.clientX / window.innerWidth * 2 - 1);
    mouseY = conf.mouseCoef * (-(e.clientY / window.innerHeight) * 2 + 1);
  });
  */
}

function animate() {
  requestAnimationFrame(animate);
  animatePaths();
}


function animatePaths() {
  const time = Date.now() * conf.timeCoef;
  const mouse = mouseX + mouseY;
  for (let i = 0; i < paths.length; i++) {
    paths[i].animate(time, mouse);
  }
}

function animateNoise() {
  if (!TweenMax.isTweening(conf)) {
    TweenMax.to(conf, 3, { noise4: -conf.noise4 });
    TweenMax.to(conf, 1.5, { heightCoef: conf.heightCoef * 1.5, repeat: 2, yoyo: true });
  }
}

function onResize() {
  $(svg).html("");
  paths=[];
 // width = r.width;
  width = $(window).width();

  //height = r.height;
  height = $(window).height();
  cx = width / 2;
  cy = height / 2;

  const nx = Math.round(width / conf.resX);
  const ny = Math.round((height - conf.heightCoef * 2) / conf.resY);
  let points, path;
  for (let j = 0; j < ny; j++) {
    points = [];
    for (let i = 0; i < nx; i++) {
      points.push([i * conf.resX, conf.heightCoef + j * conf.resY]);
    }
    points.push([width, conf.heightCoef + j * conf.resY]);
    path = new Path(svg, points, '#03345E');
    paths.push(path);
  }
  //alert($("#svg").children().length);
}

class Path {
  constructor(parent, points, stroke) {
    this.parent = parent;
    this.opoints = points;
    this.points = [];
    for (let i = 0; i < this.opoints.length; i++) {
      this.points[i] = [this.opoints[i][0], this.opoints[i][1]];
    }
    this.stroke = stroke;
    this.create();
  }
  create() {
    this.path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    this.parent.appendChild(this.path);
    this.updateStroke();
  }
  animate(time, mouse) {
    let p, x, y;
    for (let i = 0; i < this.points.length; i++) {
      p = this.opoints[i];
      this.points[i][0] = p[0];
      this.points[i][1] = p[1] + simplex.noise4D(p[0] * conf.noiseCoef + time, p[1] * conf.noiseCoef - time, null, conf.noise4) * conf.heightCoef;
    }
    this.updatePathD();
  }
  updateStroke() {
    this.path.setAttributeNS(null, 'stroke', this.stroke);
  }
  updatePathD() {
    this.path.setAttributeNS(null, 'd', this.pathD());
  }
  pathD() {
    const d = this.points.reduce((acc, point, i, a) => i === 0 ?
    `M ${point[0]},${point[1]}` :
    `${acc} L ${point[0]},${point[1]}`,
    '');

    return d;
  }}


function rnd(max, negative) {
  return negative ? Math.random() * 2 * max - max : Math.random() * max;
}

init();

$(document).ready(function(){
  
  
  displayWaves("block");


  $("#gh").click(function(){
   // $(window).scrollTo($("#home"),500);
    showSection("#home");
    displayWaves("block");

  });
  $("#n-about").click(function(){
   // $(window).scrollTo($("#about"),500);
    showSection("#about");
    displayWaves("none");
  });
  $("#n-artists").click(function(){
   // $(window).scrollTo($("#artists"),500);
    showSection("#artists");
    displayWaves("none");
  });
 
  $(window).resize(function(){
    //const r = svg.getBoundingClientRect();
 // init();
  onResize();
  })

  //b = $(window).height()-$("#artists ul").height() - 700;
  b = $("#artists ul").height() + 300;
  $("#svg-container").css ({"top":b});
      
  if ($(window).width() <= 767){
       b=300;
       $("#svg-container").css ({"top":b});
  }
  
  showSection("#home");
  $("#artists ul").css ({"opacity":"1"});

});

function showSection(s){
    $("section").removeClass("active");
    $("section").addClass("inactive");
    $(s).removeClass("inactive");
    $(s).addClass("active");
    var b;

}

function displayWaves(e){
  var ww = $(window).width();
  if (ww <= 767){
  $("#svg-container").css({"display":e});
}
}


